#
# Cookbook:: bugtick
# Recipe:: install.rb
# Description: Installs a LAMPP stack 
#	* Apache 2.4+, Mysql 5+, PHP7
#
# Copyright:: 2017, The Authors, All Rights Reserved.

#Execute update + upgrade
execute "update-upgrade" do
  command "apt-get update && apt-get upgrade -y"
  action :run
end

# Install apache2
package "apache2" do
  action :install
end

# Start apache when done
service "apache2" do
  action [:enable, :start]
end



# PHP Installation
package "php5" do
  action :install
end
package "php-pear" do
  action :install
end
package "php-mysql" do
  action :install
end


# Install GIT
package "git" do
	action :install
end
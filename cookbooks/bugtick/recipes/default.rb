#
# Cookbook:: bugtick
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

print "[SYSTEM] Updating system via apt-get update.\n"
execute "update-upgrade" do
  command "apt-get update && apt-get upgrade -y"
  action :run
end

print "[SYSTEM] Checking if Apache exist."
service "apache2" do